-- 22-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require("dprint");


describe("dprint", function()
  local output -- simulate output in StdOut
  local function print_to_ouput(line)
    output[#output + 1] = line
  end

  before_each(function()
    M.on = true -- M.enable()
    output = {}
    M.set_print(print_to_ouput)
    M.modules = {}
  end)

  --
  it("how-to-use simple", function()
    M.disable() -- the default state is disabled debug mode

    ---- module:
    local m = {} -- module
    m.dprint = M.dprint

    m.do_stuff = function(x, y)
      m.dprint('do_stuff x:', x, 'y:', y)
      -- code
      return tostring(x) .. ':' .. tostring(y)
    end
    -----

    M.enable(true)
    -- after enable in stdout print this message: with trace point from which
    -- debug mod was enabled (it can be usefull for testings)
    local exp = 'spec/dprint_spec.lua:%d+: # Debug Print Enabled'
    assert.match(exp, output[#output])

    -- call function with dprint hook inside with enabled debug mode
    assert.same('1:2', m.do_stuff(1, 2))

    -- last line in StdOut
    assert.match('spec/dprint_spec.lua:%d+: do_stuff x: 1 y: 2', output[#output])
  end)


  --
  -- The wrapper is designed to allow you to turn off the debug output of a
  -- single module,  without turning off debug mode for other modules
  it("how-to-use with wrapped dprint for module", function()
    M.disable() -- the default state is disabled debug mode

    ---- module A:
    local ma = {}
    -- create dprint wrapper with Disabled debug output by default
    ma.dprint = M.mk_dprint_for(ma, false) -- wrapper for module A

    ma.method = function(input)
      ma.dprint('A method input:', input)
      return tostring(input) -- code
    end
    -----

    ---- module B:
    local mb = {}
    mb.dprint = M.mk_dprint_for(mb) -- wrapper for module B

    mb.method = function(input)
      mb.dprint('B method input:', input)
      return tostring(input) -- code
    end
    -----

    M.enable(true)
    assert.is_false(M.is_enabled(ma)) -- module disabled in a wrapper
    assert.is_true(M.is_enabled(mb))  -- enabled(default)

    assert.match('dprint_spec.lua:%d+: # Debug Print Enabled', output[#output])
    assert.same(1, #output)        -- in stdout one line

    assert.same('8', ma.method(8)) -- disabled from start
    assert.same(1, #output)        -- in stdout one line no new messages

    assert.same('8', mb.method(8)) -- enabled from start
    assert.same(2, #output)        -- has new messages
    assert.match('spec/dprint_spec.lua:%d+: B method input: 8', output[#output])

    M.enable_module(ma)
    assert.same('16', ma.method(16))
    assert.same(3, #output)
    assert.match('spec/dprint_spec.lua:%d+: A method input: 16', output[#output])

    M.disable_modules(mb)
    assert.same('13', mb.method(13))
    assert.same(3, #output) -- no new lines in stdout
  end)
  --
  --
  it("enable_module", function()
    local module = {}
    M.enable_module(module)
    assert.same({}, module)
    assert.same(true, M.is_enabled(module))

    M.enable_module(module, false)
    assert.same(false, M.is_enabled(module))
  end)

  --
  it("enable_module", function()
    local module = {}
    M.on = false
    assert.is_false(M.is_enabled()) -- global scope

    M.mk_wrapper(module, false)
    assert.is_false(M.is_enabled(module))

    M.enable_module(module, true)
    assert.is_true(M.is_enabled(module))
    assert.is_true(M.is_enabled()) -- global
    assert.is_true(M.on)           -- global

    M.on = false
    assert.is_false(M.is_enabled(module)) -- exactly for module is on, global - off
    assert.is_false(M.is_enabled())       -- global

    M.on = true
    assert.is_true(M.is_enabled(module))
  end)

  it("enable_module", function()
    local perr = "not found not.exist.module"
    assert.match_error(function() M.is_enabled('not.exist.module') end, perr)
  end)

  it("enable_module by module itself", function()
    local module = {}
    local dprint = M.mk_dprint_for(module, true)
    dprint('debug message 1')
    assert.match('debug message 1', output[1])
    output = {}
    M.enable_module(module, false)
    dprint('debug message 2')
    assert.same(0, #output)
  end)

  it("enable_module global on", function()
    local module = {}
    local dprint = M.mk_dprint_for(module, true)
    M.on = false
    dprint('debug message 0')
    assert.same({}, output)

    M.enable_module('__dprint', false) -- self M.on -- global scope
    dprint('debug message 1')
    assert.same(0, #output)

    M.enable_module('__dprint', true)
    dprint('debug message 1')
    assert.same(1, #output)
    assert.match('debug message 1', output[1])
  end)

  --
  it("enable_all_modules status", function()
    -- modules
    local ma, mb, mc, me = { 1 }, { 2 }, { 3 }, { 4 }
    -- emulate require
    _G.package.loaded['ma'] = ma
    _G.package.loaded['mb'] = mb
    _G.package.loaded['mc'] = mc
    _G.package.loaded['me'] = me
    local _ = M.mk_dprint_for(ma, true)
    local _ = M.mk_dprint_for(mb, true)
    local _ = M.mk_dprint_for(mc, true)
    local _ = M.mk_dprint_for(me, true)

    assert.same(true, M.on)
    assert.same(true, M.modules[ma].debug_enabled)
    assert.same(true, M.modules[mb].debug_enabled)
    assert.same(true, M.modules[mc].debug_enabled)
    assert.same(true, M.modules[me].debug_enabled)
    local exp = {
      __src_root = 'nil',
      __dprint = true,
      ma = true,
      mb = true,
      mc = true,
      me = true
    }
    assert.same(exp, M.status())

    assert.same(2, M.enable_all_modules(false, { 'mb', 'mc' }))
    local exp2 = {
      __src_root = 'nil',
      __dprint = true,
      ma = false,
      mb = true,
      mc = true,
      me = false
    }
    assert.same(exp2, M.status())
  end)

  --
  it("enable_all_modules status", function()
    local ma, mb, mc = { 1 }, { 2 }, { 3 }
    -- emulate require
    _G.package.loaded['ma'] = ma
    _G.package.loaded['mb'] = mb
    _G.package.loaded['mc'] = mc
    local dprint_a = M.mk_dprint_for(ma, true)
    local dprint_b = M.mk_dprint_for(mb, true)

    dprint_a('dmsg 1_a')
    dprint_b('dmsg 2_b')
    assert.same(2, #output)
    output = {}
    assert.same(true, M.on)
    assert.same(true, M.modules[ma].debug_enabled)
    assert.same(true, M.modules[mb].debug_enabled)
    local exp = { __src_root = 'nil', __dprint = true, mb = true, ma = true }
    assert.same(exp, M.status())

    assert.same(2, M.enable_all_modules(false))
    dprint_a('dmsg 1_a')
    dprint_b('dmsg 2_b')
    assert.same(0, #output)
    assert.same(false, M.on)
    local exp2 = { __src_root = 'nil', __dprint = false, mb = false, ma = false }
    assert.same(exp2, M.status())
    assert.same(false, M.modules[ma].debug_enabled)
    assert.same(false, M.modules[mb].debug_enabled)

    assert.same(2, M.enable_all_modules(false))
    M.on = true ------ !! global on
    dprint_a('dmsg 1_a')
    dprint_b('dmsg 2_b')
    assert.same(0, #output)
    local exp3 = { __src_root = 'nil', __dprint = true, mb = false, ma = false }
    assert.same(exp3, M.status())

    assert.same(2, M.enable_all_modules(true))
    dprint_a('dmsg 1_a')
    dprint_b('dmsg 2_b')
    assert.same(2, #output)
    assert.same(true, M.on)
    local exp4 = { __src_root = 'nil', __dprint = true, mb = true, ma = true }
    assert.same(exp4, M.status())
  end)

  it("restore_from", function()
    local ma, mb, mc = { 1 }, { 2 }, { 3 }
    -- emulate require
    _G.package.loaded['ma'] = ma
    _G.package.loaded['mb'] = mb
    _G.package.loaded['mc'] = mc
    -- register in M.modules
    M.mk_dprint_for(ma, true)
    M.mk_dprint_for(mb, false)
    M.mk_dprint_for(mc, false)
    local T, F = true, false

    local state = M.status()
    local exp = { __src_root = 'nil', __dprint = T, ma = T, mb = F, mc = F }
    assert.same(exp, state)
    assert.same(T, M.is_enabled(ma))
    assert.same(F, M.is_enabled(mb))
    assert.same(F, M.is_enabled(mc))

    -- emulate reload packages
    --
    local na, nb, nc = { 11 }, { 22 }, { 32 }
    _G.package.loaded['ma'] = na
    _G.package.loaded['mb'] = nb
    _G.package.loaded['mc'] = nc

    assert.are_not_equal(_G.package.loaded['ma'], ma)
    assert.equal(_G.package.loaded['ma'], na)

    local mcnt, tmc = M.restore_from(state)
    assert.same(3, mcnt)
    assert.same(3, tmc)
    assert.same(true, M.is_enabled(na))
    assert.same(false, M.is_enabled(nb))
    assert.same(false, M.is_enabled(nc))
  end)

  it("status_of", function()
    local mod_a = { 1 }
    -- emulate require
    _G.package.loaded['mod_a'] = mod_a
    local _ = M.mk_dprint_for(mod_a, true)
    local exp = { __dprint = true, mod_a = { debug_enabled = true } }
    assert.same(exp, M.status_of('mod_a'))

    M.enable_module(mod_a, false)
    local exp2 = { __dprint = false, mod_a = { debug_enabled = false } }
    assert.same(exp2, M.status_of('mod_a'))

    M.enable_module(mod_a, true)
    local exp3 = { __dprint = true, mod_a = { debug_enabled = true } }
    assert.same(exp3, M.status_of('mod_a'))

    M.on = false
    local exp4 = { __dprint = false, mod_a = { debug_enabled = true } }
    assert.same(exp4, M.status_of('mod_a'))
  end)

  --
  it("mk_dprint_for", function()
    M.disable()

    local module = {}
    module.dprint = M.mk_dprint_for(module)
    module.func = function(x)
      -- print(debug.traceback())
      local l, t = module.dprint('func x:', x)
      -- avoid tailcall
      return l, t
    end

    M.no_stdout = true
    assert.is_function(module.dprint)
    assert.is_nil(module.func(8))

    M.enable()
    local line, trace = module.func(8)
    assert.same('func x: 8', line)
    assert.match('spec/dprint_spec.lua:%d+: ', trace)
  end)

  --
  --
  it("get_trace", function()
    assert.match('spec/dprint_spec.lua:%d+: ', M.get_trace(0))
  end)

  --
  --
  it("visualize", function()
    M.enable()
    local exp = [[
spec/dprint_spec.lua:%d+: ps:1 pe:2 sub%-len:1 str%-len:5
what: |str| 0 key: ||
abcde
^
 ^
sub:|ab|
]]
    assert.match(exp, M.visualize('abcde', 1, 2, 'what:', 'str', 0, 'key:', ''))
  end)

  --
  --
  it("visualize", function()
    local exp = [[
spec/dprint_spec.lua:%d+: pos:21 str%-len:33
what: |str| 0 key: ||
dfwejfwlhefqwluery34 yu2830h2jhf3
                    ^
]]
    local line = 'dfwejfwlhefqwluery34 yu2830h2jhf3'
    assert.match(exp, M.visualize(line, 21, 0, 'what:', 'str', 0, 'key:', ''))
  end)

  --
  --
  it("dprint", function()
    assert.same('the debug message', M.dprint('the debug message'))
  end)

  --
  --
  it("dprint", function()
    local exp = 'msg key: |value| value: 1 f: true'
    local line, trace = M.dprint('msg', 'key:', 'value', 'value:', 1, 'f:', true)
    assert.same(exp, line)
    assert.match('spec/dprint_spec.lua:%d+: ', trace)
  end)

  --
  --
  it("dprint", function()
    local exp = 'msg key: |value| value: 1 f: true'
    local line, trace = M.dprint(1, 'msg', 'key:', 'value', 'value:', 1, 'f:', true)
    assert.same(exp, line) --  ^
    assert.match('spec/dprint_spec.lua:%d+: ', trace)
  end)

  --
  --
  it("enable", function()
    assert.is_nil(M.enable(false))
    assert.is_nil(M.dprint('message'))
    assert.is_nil(M.visualize('134236427367', 4, 5))

    assert.match('# Debug Print Enabled print: ', M.enable(true))
    assert.same('message:  16', M.dprint('message: ', 16))
    assert.is_not_nil(M.visualize('134237', 4, 5))

    M.enable(false)
    finally(function()
      M.enable(false)
    end)
  end)

  --
  --
  it("mk_dprint_wrapper", function()
    M.enable()
    assert.is_true(M.on)
    local module = {}

    -- create dprint wrapper for module
    local dprint = M.mk_dprint_for(module)
    assert.same(true, M.is_enabled(module))

    local line, trace = dprint('debug message 1.') -- create debug message
    assert.same('debug message 1.', line)
    assert.match('spec/dprint_spec.lua:%d+: ', trace)

    -- make sure the message passed
    --
    assert.is_not_nil(output[1])
    local ptn = 'spec/dprint_spec.lua:%d+: # Debug Print Enabled print: '
    assert.match(ptn, output[1])
    assert.match('debug message 1.', output[2])
    assert.same(2, #output)

    -- disable output only for this module
    M.enable_module(module, false)

    dprint('debug message 2.')
    assert.same(2, #output)

    M.enable_module(module, true)

    dprint('debug message 3.')

    assert.same(3, #output)
    assert.match('debug message 3.', output[#output])

    M.enable(false) -- make sure no debug messages then debug is off
    dprint('debug message 4.')
    assert.match('debug message 3.', output[#output])
  end)


  it("find_src_root", function()
    local find_root = M.find_src_root
    local s = "@/home/user/.local/share/app/libs/lua/source.lua"
    assert.same('/home/user/.local/share/app/libs', find_root(s))

    s = "@/home/user/.local/share/app/libs/src/source.lua"
    assert.same('/home/user/.local/share/app/libs', find_root(s))

    s = "@/home/user/.local/share/app/file.lua"
    assert.same('/home/user/.local/share/app', find_root(s))

    s = "@/usr/local/share/lua/5.1/rock/builtin.lua"
    assert.same('/usr/local/share', find_root(s))

    -- win paths
    s = "@c:\\app\\libs\\src\\file.lua"
    assert.same('c:\\app\\libs', find_root(s))

    s = "@c:\\app\\libs\\file.lua"
    assert.same('c:\\app\\libs', find_root(s))
  end)


  -- tools

  --
  --
  it("diff", function()
    assert.same({}, M.diff("abc", "abc", false, {}))
    assert.same({}, M.diff("abc\nde", "abc\nde", false, {}))

    local exp = { { 2, 'de', 'def' } }
    assert.same(exp, M.diff("abc\nde", "abc\ndef", false, {}))

    exp = { { 1, 'abc', 'abC' } }
    assert.same(exp, M.diff("abc\nde\nkl", "abC\nde\nkl", false, {}))

    exp = { { 1, 'abc', 'abC' }, { 3, 'kl', 'kL' } }
    assert.same(exp, M.diff("abc\nde\nkl", "abC\nde\nkL", false, {}))

    assert.same({}, M.diff("%w+\n%w+\n%w+", "abc\nde\nkL", true, {}))
  end)

  --
  --
  it("set_silent_print", function()
    assert.is_false(M.is_silent_print())
    M.dprint('message to StdOut.')
    assert.match('message to StdOut.', output[1])

    -- turn messages into inner container without printing to StdOut(output)
    M.set_silent_print()
    assert.is_true(M.is_silent_print())

    M.dprint('new "silent" message #1.')
    local exp = 'new "silent" message #1.'
    assert.match(exp, M.get_messages()[1])

    -- back to StdOut
    assert.are_not_same(_G.print, M.print)

    M.set_silent_print(false)
    assert.same(_G.print, M.print)

    -- in testing case restore "StdOut"(output)
    M.print = print_to_ouput

    M.dprint('message to StdOut #2.')
    assert.match('message to StdOut #2.', output[2])
  end)

  --
  --
  it("dump", function()
    local t = {
      object = 'state',
      tbl = {
        func = M.get_table_hash,
      },
      func = M.get_table_hash,
    }
    t.loop = t
    t = setmetatable(t, { __index = function() end })

    local exp = [[
<table 1 0x[%w]+> {
  tbl = <table 2 0x[%w]+> {
    func = function: 0x[%w]+
  }
  loop = <table 1> {...}
  object = state
  func = function: 0x[%w]+
  <metatable 3 0x[%w]+> {
    __index = function: 0x[%w]+
  }
}
]]
    local res = M.dump(t)
    -- show diffs
    -- assert.same({}, M.diff(exp, res, true, {}))
    assert.match(exp, res)
  end)

  it("dump", function()
    local t = {
      object = 'state',
      tbl = {
        func = M.get_table_hash,
      },
      func = M.get_table_hash,
    }
    t.loop = { loop = t, tbl_copy = t.tbl }
    t = setmetatable(t, { __index = function() end })

    local exp = [[
<table 1 0x[%w]+%> {
  tbl = <table 2 0x[%w]+> {
    func = function: 0x[%w]+
  }
  loop = <table 3 0x[%w]+> {
    tbl_copy = <table 2> {...}
    loop = <table 1> {...}
  }
  object = state
  func = function: 0x[%w]+
  <metatable 4 0x[%w]+> {
    __index = function: 0x[%w]+
  }
}
]]
    local res = M.dump(t)
    -- show diffs
    -- assert.same({}, M.diff(exp, res, true, {}))
    assert.match(exp, res)
  end)


  it("inspect", function()
    assert.same('<1>{skey = "s-val"}', M.inspect({ skey = 's-val', }))
    assert.same('<1>{[1] = "num"}', M.inspect({ [1] = 'num', }))
    assert.same('<1>{[1] = 8}', M.inspect({ [1] = 8, }))
    assert.same('<1>{[1] = <function 1>}', M.inspect({ [1] = M.get_trace, }))
    assert.same('<1>{[<function 1>] = 8}', M.inspect({ [M.get_trace] = 8 }))
    assert.same('<1>{[<table 2>] = <3>{}}', M.inspect({ [{}] = {} }))
  end)

  it("inspect", function()
    local t = { [M.get_trace] = 'key-is-func', }
    local exp = '<1>{[<function 1>] = "key-is-func"}'
    assert.same(exp, M.inspect(t))
  end)


  local fecho = function(...)
    local s = ''
    for i = 1, select('#', ...) do
      s = s .. ' #' .. tostring(i) .. ':' .. tostring(select(i, ...))
    end
    if #s == 0 then s = 'empty' end
    return s
  end

  it("fecho", function()
    assert.same('empty', fecho())
    assert.same(' #1:nil', fecho(nil))
    assert.same(' #1:nil #2:2', fecho(nil, 2))
    assert.same(' #1:a #2:b #3:c #4:d', fecho('a', 'b', 'c', 'd'))
    assert.same(' #1:a #2:b #3:nil #4:d', fecho('a', 'b', nil, 'd'))
  end)


  it("function_call: callback to dprint", function()
    local f = function(...)
      local i, val = M.function_call(select(1, ...), 1, ...)
      local next = i and select(i + 1, ...) or '?'
      return tostring(i) .. '|' .. tostring(val) .. '|' .. tostring(next)
    end
    assert.same('1|empty|word', f(fecho, 'word')) -- witout args
    assert.same('4| #1:A|word', f(fecho, '(', 'A', ')', 'word'))
    assert.same('3|empty|word', f(fecho, '(', ')', 'word'))
    assert.same('1|empty|(A)', f(fecho, '(A)', 'word')) -- witout args

    --      next i|     | .-arg[i]
    assert.same('4| #1:a|NEXT', f(fecho, '(', 'a', ')', 'NEXT'))
    assert.same('4| #1:a|NEXT', f(fecho, '(', 'a', ')', 'NEXT', 11, 12))
    assert.same('5| #1:a #2:nil|?', f(fecho, '(', 'a', nil, ')'))
    assert.same('6| #1:a #2:nil #3:c|?', f(fecho, '(', 'a', nil, 'c', ')'))
    assert.same('6| #1:a #2:nil #3:c|8', f(fecho, '(', 'a', nil, 'c', ')', 8))
    assert.same('7| #1:1 #2:2 #3:3 #4:4|A', f(fecho, '(', 1, 2, 3, 4, ')', 'A'))

    local exp = '8| #1:1 #2:2 #3:3 #4:4 #5:5|A'
    assert.same(exp, f(fecho, '(', 1, 2, 3, 4, 5, ')', 'A'))

    exp = '9| #1:1 #2:2 #3:3 #4:4 #5:5 #6:6|A'
    assert.same(exp, f(fecho, '(', 1, 2, 3, 4, 5, 6, ')', 'A'))

    exp = '10| #1:1 #2:2 #3:3 #4:4 #5:5 #6:6 #7:7|A'
    assert.same(exp, f(fecho, '(', 1, 2, 3, 4, 5, 6, 7, ')', 'A'))

    exp = '11| #1:1 #2:2 #3:3 #4:4 #5:5 #6:6 #7:7 #8:8|A'
    assert.same(exp, f(fecho, '(', 1, 2, 3, 4, 5, 6, 7, 8, ')', 'A'))

    -- max is 8 paramsk parameters greater than 8 are simply discarded
    exp = '12| #1:1 #2:2 #3:3 #4:4 #5:5 #6:6 #7:7 #8:8 ..(ignored params:1)|A'
    assert.same(exp, f(fecho, '(', 1, 2, 3, 4, 5, 6, 7, 8, 9, ')', 'A'))

    -- broken - missed closed ')' show all args to end
    assert.same('7| #1:a #2:10 #3:11 #4:12|?', f(fecho, '(', 'a', 10, 11, 12))

    exp = '13| #1:1 #2:2 #3:3 #4:4 #5:5 #6:6 #7:7 #8:8 ..(ignored params:2)|?'
    assert.same(exp, f(fecho, '(', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
  end)


  local build_line_wrapper = function(...)
    local _, line = M.build_line(...)
    return tostring(line)
  end
  -- feature:
  -- Supports callback functions with a variable number of parameters
  it("build_line with functions", function()
    local f = build_line_wrapper
    assert.same('msg |a| |b| |c|', f('msg', 'a', 'b', 'c'))

    local exp = 'msg empty |next|'
    --                      ^^^^ word after callback and params for it
    --               ^^^^^ - output from callback(fecho)
    assert.same(exp, f('msg', fecho, '(', ')', 'next'))
    --                        ^ callback

    assert.same('msg empty', f('msg', fecho))

    exp = 'msg  #1:PARAM |next|'
    assert.same(exp, f('msg', fecho, '(', 'PARAM', ')', 'next'))

    exp = 'msg  #1:b #2:c |next|'
    assert.same(exp, f('msg', fecho, '(', 'b', 'c', ')', 'next'))

    exp = 'msg  #1:1 #2:2 #3:3 #4:4 |next-arg|'
    assert.same(exp, f('msg', fecho, '(', 1, 2, 3, 4, ')', 'next-arg'))

    exp = 'm  #1:1 #2:2 #3:3 #4:4 #5:5 #6:6 #7:7 #8:8 K: |V|'
    assert.same(exp, f('m', fecho, '(', 1, 2, 3, 4, 5, 6, 7, 8, ')', 'K:', 'V'))

    -- max 8 params, all others are simply discarded
    exp = 'm  #1:1 #2:2 #3:3 #4:4 #5:5 #6:6 #7:7 #8:8 ..(ignored params:1) |NA|'
    assert.same(exp, f('m', fecho, '(', 1, 2, 3, 4, 5, 6, 7, 8, 9, ')', 'NA'))

    exp = 'm  #1:1 #2:2 #3:3 #4:4 #5:5 #6:6 #7:7 #8:8 ..(ignored params:2) |NA|'
    assert.same(exp, f('m', fecho, '(', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ')', 'NA'))

    exp = ' #1:1 #2:2 #3:3 #4:4 #5:5 #6:6 #7:7 #8:8 ..(ignored params:2) K: |V|'
    assert.same(exp, f(fecho, '(', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ')', 'K:', 'V'))
  end)


  it("build_line with functions :dump: (hash)", function()
    local f = build_line_wrapper
    assert.match('msg empty', f('msg', fecho)) -- call the function

    -- print hash instead of calling function
    assert.match('msg :dump: function: 0x[%w]+', f('msg', ':dump:', fecho))
    assert.match('msg :tostring: function: 0x.*', f('msg', ':tostring:', fecho))
  end)


  -- apply inspect to the result of the function
  it("build_line with functions :inspect:", function()
    local f = build_line_wrapper
    local function func(t) return t end

    -- if result of the function is table: will be printed only type +0xHash
    assert.match('msg table: 0x[%w]+', f('msg', func, '(', { 8, k = 'v' }, ')'))

    -- way to show content of the table via inspect
    local exp = 'msg :inspect: <1>{[1] = 8, [2] = "16", key = 64}'
    assert.same(exp, f('msg', ':inspect:', func, '(', { 8, '16', key = 64 }))
  end)


  it("build_line with functions :dump: (hash)", function()
    local f = build_line_wrapper
    assert.same('m |abcdefg1234|', f('m', 'abcdefg1234'))
    assert.same('msg abcd..(3)', f('msg', ':limit:', 4, 'abcdefg'))
    -- defualt limit
    local exp = 'msg #############################32#..(5)'
    assert.same(exp, f('msg', ':limit:', '#############################32######'))

    exp = 'msg ################..(15)'
    assert.same(exp, f('msg', ':limit:', 16, '#############################32'))

    assert.same('msg  21', f('msg', ':limit:', 16, 21))
    assert.same('msg  nil nil', f('msg', ':limit:', nil, nil))
    assert.same('msg ', f('msg', ':limit:'))

    assert.match('msg  table: 0x.*', f('msg', ':limit:', { 1, 2, 3, 4, 5 }))
  end)

  it("filter_modules", function()
    local f = M.find_modules_by_mask

    _G.package.loaded['stub.os.OSystem'] = { 'mod1' }
    _G.package.loaded['stub.os.FileStub'] = { 'mod2' }
    _G.package.loaded['stub.os.FileSystem'] = { 'mod3' }

    assert.same({ { 'mod1' }, { 'mod2' }, { 'mod3' } }, f('stub.os.*'))
    assert.same({ { 'mod1' }, { 'mod2' }, { 'mod3' } }, f('stub.*'))
    assert.same({ { 'mod1' }, { 'mod2' }, { 'mod3' } }, f('stu*'))
    assert.same({}, f('stux.*'))
  end)

  it("apply_for_each", function()
    local list = { { 'mod1' }, { 'mod2' }, { 'mod3' } }
    local cb = function(t) t[1] = string.upper(t[1]) end
    assert.same(true, M.apply_for_eachv(list, cb))
    assert.same({ { 'MOD1' }, { 'MOD2' }, { 'MOD3' } }, list)
  end)

  it("disable_modules & enable_modules by mask", function()
    -- prepare fake modules
    local m1, m2, m3 = { 1 }, { 2 }, { 3 }
    _G.package.loaded['stub.os.OSystem'] = m1
    _G.package.loaded['stub.os.FileStub'] = m2
    _G.package.loaded['stub.os.FileSystem'] = m3
    assert.same(false, M.is_enabled(m1))
    assert.same(false, M.is_enabled(m2))
    assert.same(false, M.is_enabled(m3))

    -- -- register in dprint and set debug == true for each module
    M.apply_for_eachv({ m1, m2, m3 }, M.mk_dprint_for)

    assert.same(true, M.is_enabled(m1))
    assert.same(true, M.is_enabled(m2))
    assert.same(true, M.is_enabled(m3))

    M.disable_modules('stub.os.*') --      payload
    assert.same(false, M.is_enabled(m1))
    assert.same(false, M.is_enabled(m2))
    assert.same(false, M.is_enabled(m3))

    M.enable_module('stub.os.*', true)
    assert.same(true, M.is_enabled(m1))
    assert.same(true, M.is_enabled(m2))
    assert.same(true, M.is_enabled(m3))

    M.enable_module('stub.os.*', false)
    assert.same(false, M.is_enabled(m1))
    assert.same(false, M.is_enabled(m2))
    assert.same(false, M.is_enabled(m3))
  end)
end)
