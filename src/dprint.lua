-- 28-10-2023 @author Swarg 22-11-2023 to separate rock-package
local M = {}
--------------------------------------------------------------------------------
--                             DEBUG PRINT
--------------------------------------------------------------------------------

-- debug mode for global scope
-- to enable use M.enable()
--
-- when dprin is triggered, this value is first checked, then whether the
-- module itself for which the wrapper was created is enabled
-- (if wrapped dprint is used)
M.on = false

-- modules for which was created dprint wrappers
-- used to disable each individual module independently of the others modules
M.modules = {} -- on/off settings for each wrapped dprint for specific module

-- default method to print into StdOut
M.print = _G.print
-- you can either override it with your own function or drown it out in confusion


M._URL = 'https://gitlab.com/lua_rocks/dprint'
M._VERSION = 'dprint 0.5.0'

local E = {}
local DEFAULT_LIMIT = 32
local WIDTH_WINDOW = 80 -- visualize

--
-- enable debug mode for all dprint - wrappers (modules)
--
---@param enable boolean|nil
function M.enable(enable)
  if enable == false then
    M.on = false
  else
    M.on = true -- default is true
  end
  if M.on then
    local readable = 'nil'
    if M.print == _G.print then
      readable = '_G.print'
    elseif M.print then
      readable = tostring(M.print) -- 'custom'
    end
    return M.dprint(1, '# Debug Print Enabled print: ', readable)
  end
end

function M.disable()
  if M.on then
    M.on = false
    return M.dprint(1, '# Debug Print Disabled')
  end
  M.on = false
end

---@param module string|table
local function get_module_state(module)
  local mod_type = type(module)
  if mod_type == 'string' then
    local ok, mod = pcall(require, module)
    if ok then
      module = mod
      mod_type = type(module)
    else
      error('not found ' .. tostring(module) .. "\n" .. tostring(mod))
    end
  end

  assert(mod_type == 'table', 'module must be a table')
  M.modules[module] = M.modules[module] or {}

  return M.modules[module]
end

---@param module table|string module itself or its full-name for require
---@param enable boolean? default value is true
local function enable_module_debug(module, enable)
  get_module_state(module).debug_enabled = (enable ~= false)
end

--
-- if the module is a nil(no params) - return the debug mode for a global scope
--
-- if specified module is a table - will return true if debugging is enabled
-- exactly for this module and
--
-- if the given value of module is a string, then this function will try to
-- require this module and if it doesn't exist, will simply return false without
-- any notification
--
-- throws errors:
-- if module is not a table or if no module was found for the given string name
--
---@param module table|string?
function M.is_enabled(module)
  if not M.on then return false end -- disabled for global scope

  if module == nil then
    return M.on
  end

  return get_module_state(module).debug_enabled == true
end

--
-- checks if there is at least one module with enabled debug print mode
---@return boolean
function M.has_enabled_module()
  for _, module_state in pairs(M.modules) do
    if module_state.debug_enabled then
      return true
    end
  end
  return false
end

--
-- generate readable status  module-name -> debug_enabled
-- module names take from _G.package.loaded
--
---@return table{string, boolean}
function M.status()
  local t = {}
  for mod_name, mod in pairs(_G.package.loaded) do
    local state = M.modules[mod]
    if state then
      t[mod_name] = state.debug_enabled
    end
  end
  t.__dprint = M.on
  t.__src_root = M.src_root or 'nil'
  return t
end

-- back all settings from saved state
-- usecase: after reload packages when the refs to a mod-tables is changed
-- Usage Example:
--   _G.__dprint_state = require('dprint').status()
--   .. reload modules-packages in runtime
--   require('dprint').restore_from(_G.__dprint_state)
--   _G.__dprint_state = nil
---@param state table
function M.restore_from(state)
  assert(type(state) == 'table', 'state')

  M.on = state.__dprint
  M.src_root = state.__src_root
  state.__dprint = nil
  state.__src_root = nil
  M.modules = {}
  local up, t = 0, 0

  for name, enabled in pairs(state) do
    t = t + 1
    local mod = _G.package.loaded[name]
    if mod then
      M.modules[mod] = { debug_enabled = enabled }
      -- print('[DEBUG] ' .. tostring(name) .. ' '
      --   .. tostring((M.modules[mod] or E).debug_enabled))
      up = up + 1
    end
  end
  return up, t
end

-- status of one given module(by fullname of by table itself)
---@param mod string|table
function M.status_of(mod)
  local st = get_module_state(mod)
  if type(mod) == 'table' then
    mod = M.find_module_name(mod) or 'unknown'
  end
  -- global_enabled
  return { __dprint = M.on, [mod] = st }
end

-- find at _G.package.loaded
---@param tmod table
function M.find_module_name(tmod)
  for mod_name, mod in pairs(_G.package.loaded) do
    if mod == tmod then
      return mod_name
    end
  end
end

-- create mapping with keys: module itself(table) -> value: module-name
---@return table{tbl, string}
function M.mk_map_tmod_to_name()
  local t = {}
  for mod_name, mod in pairs(_G.package.loaded) do
    if M.modules[mod] then
      t[mod] = mod_name
    end
  end
  return t
end

--
-- turn swith for debug specified module
-- UseCase: be able to turn off the debug output for one specific module
-- for M.mk_dprint_wrapper()
--
---@param module table|string
---@param enable boolean? defualt is true
function M.enable_module(module, enable)
  local mtype = type(module)
  assert(mtype == 'table' or mtype == 'string', 'module itself or fullname')
  enable = enable == nil and true or enable -- default is true
  assert(type(enable) == 'boolean', 'enable')

  if module == '__dprint' then -- self
    M.on = enable == true
    return
  end

  if mtype == 'string' and module:sub(-1, -1) == '*' then -- like stub.os.*
    local t = M.find_modules_by_mask(module)
    M.apply_for_eachv(t, function(modt)
      enable_module_debug(modt, enable)
    end)
  else
    enable_module_debug(module, enable)
  end

  -- M.on = (enable == true) or M.has_enabled_module() or false
  if enable then M.on = true else M.on = M.has_enabled_module() end
end

--
-- set debug print enabled for all modules [excepts given names]
--
---@param enabled boolean  -- true - debug-print is enabled
---@param excepts table?   -- list of module names that state of which
--                            should not be changed(just don't touch them)
-- Note: excepts should contains the full names of modules(used in reguired)
-- this function do NOT support direct processing of the modules themselves
--
---@return number - number of modules for which the "enabled" was applied
---@return number - number of total registered modules
function M.enable_all_modules(enabled, excepts)
  local n, t, map, exc_names = 0, 0, nil, nil

  -- if has list of excepted module names -
  if excepts and #excepts > 0 then
    map = M.mk_map_tmod_to_name()
    exc_names = {}
    -- build set mod_name -> boolean (to speed up the search)
    for _, mod_name in ipairs(excepts) do
      exc_names[mod_name] = true
    end
    for mod, module_state in pairs(M.modules) do
      t = t + 1
      if not exc_names[map[mod] or false] then
        module_state.debug_enabled = enabled
        n = n + 1
      end
    end
    -- just simple apply for all registered modules
  else
    for _, module_state in pairs(M.modules) do
      module_state.debug_enabled = enabled
      n = n + 1
    end
  end
  if n == t then
    M.on = enabled
  else
    M.on = M.has_enabled_module()
  end
  return n, t
end

--
-- find already loaded modules (_G.package.loaded) by given mask
-- stub.os.* -> to list of modules itself {}
--
---@param mask string
---@return table - "list" of founded modules itself(tables)
function M.find_modules_by_mask(mask)
  local i = string.find(mask, '*')
  assert(i, 'expected valid filter with * in the end got: ' .. tostring(mask))

  local pkg = string.sub(mask, 1, i - 1)
  local lpkg = #pkg
  local t = {}

  for key, value in pairs(_G.package.loaded) do
    if type(key) == 'string' and key:sub(1, lpkg) == pkg then
      t[#t + 1] = value
    end
  end

  return t
end

--
-- call given func for each value in t-table
--
---@param t table?
---@param func function
function M.apply_for_eachv(t, func)
  if t then
    for _, modt in pairs(t) do func(modt) end
    return true
  end
  return false
end

--
function M.disable_modules(...)
  for i = 1, select('#', ...) do
    local module = select(i, ...)
    local tm = type(module)
    if tm == 'table' then
      enable_module_debug(module, false)
      --
    elseif tm == 'string' and module:sub(-1, -1) == '*' then
      local t = M.find_modules_by_mask(module) -- like stub.os.*
      M.apply_for_eachv(t, function(modt)
        enable_module_debug(modt, false)
      end)
    end
  end
end

-- _G.print is defualt
function M.set_print(print)
  M.print = print
end

function M.set_debug(enable)
  M.enable(enable)
end

--------------------------------------------------------------------------------
--                             Silent Print
--------------------------------------------------------------------------------

M.messages = nil -- used to collect debug messages by silent_print

-- way to collect dprint messages into inner list without printing into StdOut

local function silent_print(...)
  -- M.messages = M.messages or {}
  local line = ''
  for i = 1, select('#', ...) do
    line = line .. ' ' .. select(i, ...)
  end
  if line:sub(-1, -1) == "\n" then
    line = line:sub(1, -1)
  end
  M.messages[#M.messages + 1] = line
end

--
---@param enable boolean?
function M.set_silent_print(enable)
  if enable or enable == nil then -- defualt is true
    M.messages = M.messages or {}
    M.print = silent_print
  else
    M.print = _G.print
  end
end

---@return boolean
function M.is_silent_print()
  return M.print == silent_print
end

--
-- return existed collected dprint messages and clear
---@return table?
function M.get_messages()
  return M.messages
end

-- return existed collected dprint messages and clear
---@return table?
function M.pop_messages()
  local m = M.messages
  M.messages = {}
  return m
end

--
-- Create wrapper of the dprint function for specified module
-- to be able to turn off the debug output for this specific module
--
-- (via M.enable_module(module, false) or
-- dprint.modules[M].debug_enabled = true
--
---@param module table - the module for which the new function is intended
---@param enable boolean? default is true
function M.mk_dprint_for(module, enable)
  assert(type(module) == 'table', 'module must be a table')

  enable = enable == nil and true or enable -- default is on
  enable_module_debug(module, enable)

  local dprint0 = function(...)
    local module0 = module
    if M.on and (M.modules[module0] or E).debug_enabled then
      local offset, line = M.build_line(...)
      local trace = M.get_trace(offset + 1)

      if M.print then
        if line:sub(1, 1) == "\n" then
          trace = "\n" .. trace
          line = line:sub(2, #line)
        end
        M.print(trace .. line)
      end

      return line, trace
    end
  end
  return dprint0
end

M.mk_wrapper = M.mk_dprint_for


-- wrapper for specified module for visualize
-- str, p_start, p_end, ...
function M.mk_dvisualize_for(module, enable)
  assert(type(module) == 'table', 'module must be a table')

  enable = enable == nil and true or enable -- default is on
  enable_module_debug(module, enable)

  ---@param str string source string
  ---@param s number start pos
  ---@param e number end pos
  local dvisualize0 = function(str, s, e, ...)
    local module0 = module
    if M.on and (M.modules[module0] or E).debug_enabled then
      return M.visualize(str, s, e, ...)
    end
  end

  return dvisualize0
end

--------------------------------------------------------------------------------

--
-- make paths to sources more readable
-- remove specified source root and
-- clears "@./" from the beginning of lines
-- (to provide one result for run tests via busted with diff paths )
--
---@param source string
---@param src_root string
---@return string
function M.mk_short_source(source, src_root)
  if src_root and src_root ~= '' then
    local i, j
    local fc2 = source:sub(1, 2)
    if fc2 == '@.' then
      i, j = 3, 3
    elseif src_root == '.' and fc2 ~= '@.' then
      i, j = 2, 2
    else
      i, j = string.find(source, src_root, 1, true)
    end
    if i and j and j > 0 then
      local d0 = string.sub(source, j + 1, j + 5)
      if d0 == '/lua/' or d0 == '/src/' then -- todo win \\
        j = j + 6                            -- skip '/lua/'
      end
      return source:sub(j)
    end
  end

  if source then
    if source:sub(1, 3) == '@./' then
      source = source:sub(4)
    elseif source:sub(1, 1) == '@' then
      source = source:sub(2)
    end
  end

  return source or ''
end

-- function M.mk_short_source(source, src_root)
--   if source then
--     if src_root and src_root ~= '' then
--       local i, j = string.find(source, src_root, 1, true)
--       if i and j and j > 0 then
--         local d0 = string.sub(source, j + 2, j + 4)
--         if d0 == 'lua' or d0 == 'src' then -- skip '/lua/'
--           j = j + 6
--         end
--         return source:sub(j)
--       end
--     elseif source:sub(1, 1) == '@' then
--       return source:sub(2)
--     end
--   end
--   return source or ''
-- end


---@param obj table
local function apply_to_array(obj)
  obj = obj:toArray()
  local s = ''
  for k, v in pairs(obj) do
    s = s .. tostring(k) .. ': ' .. tostring(v) .. ', '
  end
  return s
end

---@return number
---@return string
function M.build_line(...)
  local offset, line, prev_key = 0, '', nil

  local i = 1
  while i <= select('#', ...) do
    local val = select(i, ...)
    if i == 1 and type(val) == 'number' then
      offset = val
    else
      local not_first = false
      if offset == 0 and i > 1 or i > 2 then
        not_first = true
        line = line .. ' '
      end
      local vt = type(val)

      if not_first and vt == 'string' then
        if val == ':limit:' then
          local a0, a1 = select(i + 1, ...)
          local n = tonumber(a0)
          if n then
            i = i + 1
          else
            n = DEFAULT_LIMIT
            a1 = a0
          end
          if type(a1) == 'string' then
            i = i + 1
            val = a1
            if #a1 > n then
              val = (a1:sub(1, n) .. '..(' .. tostring(#a1 - n) .. ')')
            end
            line = line .. val
          else
            val = '' -- next step
          end
          --
        elseif val ~= "\n" and (val:sub(-1, -1) ~= ':' or #val == 1) then
          line = line .. "|" .. val .. "|"
        else
          line = line .. tostring(val)
          prev_key = val
        end
        --
      else
        if vt == 'table' then
          if prev_key == ':inspect:' then
            val = M.inspect(val)
          elseif prev_key == ':dump:' then
            val = M.dump(val)
          elseif type(val.__tostring) == 'function' then
            val = val.__tostring(val)
          elseif type(val.toArray) == 'function' then
            val = apply_to_array(val)
          end
          --
        elseif vt == 'function' then
          if prev_key == ':dump:' then
            val = M.dump(val)
          elseif prev_key ~= ':tostring:' then
            -- case: dprint('msg', func, '(', 1, 2, ')', next-arg, ... )
            i, val = M.function_call(val, i, ...)
            if prev_key == ':inspect:' then -- inspect the result of the func
              val = M.inspect(val)
            end
          end
        end
        line = line .. tostring(val)

        if vt == 'string' then prev_key = val end
      end
    end
    i = i + 1
  end
  return offset, line
end

---@return number
---@return string?
function M.function_call(func, i, ...)
  assert(type(func) == 'function', 'expect function')
  local next = select(i + 1, ...)
  local val = ''
  if type(next) == 'string' and next == '(' then
    local cnt = 0
    -- find end of params for function:  func '(', param1, param2, param3, ')'
    for j = i + 2, select('#', ...) do
      if select(j, ...) == ')' then
        break
      end
      cnt = cnt + 1
    end

    if cnt == 0 then
      val = func()
    elseif cnt == 1 then
      local a1 = select(i + 2, ...)
      val = func(a1)
    elseif cnt == 2 then
      local a1, a2 = select(i + 2, ...)
      val = func(a1, a2)
    elseif cnt == 3 then
      local a1, a2, a3 = select(i + 2, ...)
      val = func(a1, a2, a3)
    elseif cnt == 4 then
      local a1, a2, a3, a4 = select(i + 2, ...)
      val = func(a1, a2, a3, a4)
    elseif cnt == 5 then
      local a1, a2, a3, a4, a5 = select(i + 2, ...)
      val = func(a1, a2, a3, a4, a5)
    elseif cnt == 6 then
      local a1, a2, a3, a4, a5, a6 = select(i + 2, ...)
      val = func(a1, a2, a3, a4, a5, a6)
    elseif cnt == 7 then
      local a1, a2, a3, a4, a5, a6, a7 = select(i + 2, ...)
      val = func(a1, a2, a3, a4, a5, a6, a7)
    elseif cnt >= 8 then
      local a1, a2, a3, a4, a5, a6, a7, a8 = select(i + 2, ...)
      val = func(a1, a2, a3, a4, a5, a6, a7, a8)
    end
    if cnt > 8 then
      val = val .. ' ..(ignored params:' .. tonumber(cnt - 8) .. ')'
    end

    return (i + 2 + cnt), val -- i+2 for build_line for while-loop with i++
  end
  val = func()

  return i, val
end

---@param str string source string
---@param s number start pos
---@param e number end pos
--- param what string|nil
--- param extra any|nil
function M.visualize(str, s, e, ...)
  if M.on then
    assert(type(str) == 'string', 'str')
    assert(type(e) == 'number', 'e')
    assert(type(s) == 'number', 's')

    local offset, info = M.build_line(...)
    local line = M.get_trace(offset + 1)

    local cursor_ps, cursor_pe = '^', ''
    -- for big str
    if s > WIDTH_WINDOW and #str > WIDTH_WINDOW then
      local hide = s - 20
      str = '..' .. str:sub(hide + 3)
      s, e = s - hide, e - hide
    end
    if #str > WIDTH_WINDOW then
      str = str:sub(1, WIDTH_WINDOW - 2) .. '..'
    end

    cursor_ps = string.format('%' .. (s - 1) .. 's^', '')
    local has_end = e and e > 0

    if has_end then
      cursor_pe = string.format('%' .. (e - 1) .. 's^', '')
      line = line ..
          'ps:' .. s .. ' pe:' .. e ..
          ' sub-len:' .. tostring(e - s) .. ' str-len:' .. tostring(#str) ..
          "\n" .. info .. "\n" ..
          str:gsub("\n", ' ') .. "\n" ..
          cursor_ps .. "\n" .. cursor_pe .. "\n"
    else
      line = line ..
          'pos:' .. s .. ' str-len:' .. tostring(#str) ..
          "\n" .. info .. "\n" ..
          str:gsub("\n", ' ') .. "\n" ..
          cursor_ps .. "\n"
    end

    if e and e > 0 then
      line = line .. "sub:|" .. str:sub(s, e) .. "|\n"
    end

    if M.print then M.print(line) end
    return line
  end
end

--
--
---@param offset number
function M.get_trace(offset)
  local src, lnum = "", 0
  local info = debug.getinfo(2 + offset, "Sl")      -- +1 on each proxy func
  if info and info.source and info.currentline then -- short_src
    lnum = info.currentline
    -- fix deep
    if lnum and lnum == -1 and offset > 1 then
      local info2 = debug.getinfo(2 + offset - 1, "Sl")
      if info2 then info = info2 end
      lnum = info.currentline
    end
    -- src = (info.source or ''):sub(2)                -- remove '@'
    src = M.mk_short_source(info.source, M.src_root)
  end
  return src .. ':' .. tostring(lnum) .. ': '
end

--
--
-- debug print
function M.dprint(...)
  if M.on == true then
    local offset, line = M.build_line(...)
    local trace = M.get_trace(offset + 1)
    if M.print then
      if line:sub(1, 1) == "\n" then
        trace = "\n" .. trace
        line = line:sub(2, #line)
      end
      M.print(trace .. line)
    end
    -- line and trace separetly
    return line, trace
  end
end

--
-- find root directory for the current application
-- by source path(for testing) of from debug.getinfo
-- usecase:
--   to make trace messages more readable
--
---@param source string?
function M.find_src_root(source, offset)
  offset = offset or 0
  if not source then
    local info, n = nil, 32
    while not info and offset >= 0 and n > 0 do
      info = debug.getinfo(2 + offset + 1, "S") -- + 1 on each proxy func
      if info and info.source then
        source = info.source
      end
      offset = offset - 1
      n = n - 1
    end
  end
  local src_root = source and (source:match('^@?(.+)[/\\]lua[/\\]') or
    source:match('^@?(.+)[/\\]src[/\\]') or
    source:match('^@?(.+)[/\\]'))
  return src_root
end

--------------------------------------------------------------------------------

---@param f function
---@param passed table
local function i_func2str(f, passed)
  passed.__funcs = passed.__funcs or {}
  passed.__funcs.__counter = passed.__funcs.__counter or 0
  if not passed.__funcs[f] then
    passed.__funcs.__counter = passed.__funcs.__counter + 1
    passed.__funcs[f] = passed.__funcs.__counter
  end
  return '<function ' .. tostring(passed.__funcs[f]) .. '>'
end

local function i_tbl2str(t, passed)
  local has = true
  passed.__counter = passed.__counter or 0

  if not passed[t] then
    passed.__counter = passed.__counter + 1
    passed[t] = passed.__counter
    has = false
  end
  -- return '<' ..pref.. tostring(passed[t]) .. '>', has
  return passed[t], has
end

--
--
-- dump object value to string
-- (replace actual table hashes by unique numbers)
--
-- WARN!
-- This function is Not intended to be used in tests to check output values.
-- Because it does not sort the keys and,
-- the order of the same values can change each time.
--
-- tables prints in simple in one-line without newlines
--
---@param passed table|nil
function M.inspect(obj, passed)
  local obj_type = type(obj)

  if obj_type == 'table' then
    passed = passed or {}

    local tn, has = i_tbl2str(obj, passed)
    if has then
      return '<table ' .. tostring(passed[tn]) .. '>'
    end

    local first, s = true, '<' .. tostring(tn) .. '>{'

    for k, v in pairs(obj) do
      if not first then
        s = s .. ', '
      end
      local tk = type(k)
      if tk == 'number' then
        k = '[' .. k .. ']'
        --
      elseif tk == 'function' then
        k = '[' .. i_func2str(k, passed) .. ']'
        --
      elseif tk == 'table' then
        k = '[<table ' .. tostring(i_tbl2str(k, passed)) .. '>]'
        --
      elseif tk ~= 'string' then
        -- Note inspect shown srting keys starts with digit as ["N*"]
        k = tostring(k)
      end
      s = s .. k .. ' = ' .. M.inspect(v, passed)
      first = false
    end

    return s .. '}'
    --
  elseif obj_type == 'string' then
    return '"' .. obj .. '"'
    --
  elseif obj_type == 'function' then
    return i_func2str(obj, passed or {})
  else
    return tostring(obj)
  end
end

--
-- TODO find way to get table "hash" without reset metatable
--
---@param t table
---@return string
function M.get_table_hash(t)
  -- a trick to avoid looping if has __tostring
  local temp, s = getmetatable(t), nil
  setmetatable(t, nil)  -- debug.setmetatable
  s = tostring(t)
  setmetatable(t, temp) -- back

  s = s:gsub('table: 0x', '', 1)
  return s
end

--
-- dump object to string with specific "hashes"
-- to get names and specific runtime addresses(hashes)
--
---@param obj any
function M.dump(obj, tab, passed, pref)
  local t = type(obj)
  passed = passed or {}
  passed.__counter = passed.__counter or 0

  tab = tab or ''
  pref = pref or ''

  if t == 'table' then
    local hash, has = '', true
    if not passed[obj] then
      passed.__counter = passed.__counter + 1
      passed[obj] = passed.__counter
      hash = ' 0x' .. tostring(M.get_table_hash(obj))
      has = false
    end
    local s = "<" .. pref .. "table " .. tostring(passed[obj]) .. hash .. "> {"
    if has then
      return s .. "...}\n"
    end
    s = s .. "\n"

    -- passed[obj] = passed.__counter
    local ltab = tab
    tab = tab .. '  '
    for k, v in pairs(obj) do
      local tv = type(v)
      s = s .. tab .. tostring(k) .. " = "
      if tv == 'table' then
        s = s .. M.dump(v, tab, passed)
      else
        s = s .. tostring(v) .. "\n"
      end
    end

    local mt = getmetatable(obj)
    if type(mt) == 'table' then
      local n = passed[mt]
      if not n then
        s = s .. tab .. M.dump(mt, tab, passed, "meta")
      else
        s = s .. tab .. '<metatable ' .. tostring(n) .. "> {..}\n"
      end
    end

    return s .. ltab .. "}\n"
  end
  return tostring(obj)
end

--
--
-- diff_lines
--
-- a simple line-by-line string comparison tool
-- split to lines and compare line-by-line
-- either for equality (when pattern is false) or for matching a given pattern
--
-- simple line-by-line comparison compares strings directly without searching
-- for gaps and intsertions of new lines
--
-- Goal: helper for testing to find not mathed line in a big multiline text
--
---@param a any (string)  if pattern == true then consider this a pattern
---@param b any (string
---@param pattern boolean?
---@param out table? if given then fill diff_lines and return it
function M.diff(a, b, pattern, out)
  local i1, i2, p1, p2, diffs, work, line = nil, nil, 1, 1, 0, true, 0
  a, b = tostring(a), tostring(b)
  pattern = pattern or false

  while work do
    line = line + 1
    i1 = a:find("\n", p1, true)
    i2 = b:find("\n", p2, true)
    if not i1 then
      i1 = #a + 1
      work = false
    end
    if not i2 then
      i2 = #b + 1
      work = false
    end

    local l1, l2 = a:sub(p1, i1 - 1), b:sub(p2, i2 - 1)

    local diff = false
    -- in string.match first is pattern
    if pattern then
      diff = string.match(l2, l1) == nil
    else
      diff = l1 ~= l2
    end

    if diff then
      if out then
        out[#out + 1] = { line, l1, l2 }
      elseif M.print then
        M.print('[DIFF]', l1, ' : ', l2)
      end
      diffs = diffs + 1
    end

    p1, p2 = i1 + 1, i2 + 1
  end
  return out or diffs > 0
end

return M
