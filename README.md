## Debug print

## Overview

The simplest debugging information logger.
Disabled by default, with ability to enable debug mode at any time.
When debug mode is enabled, data is simply output to StdOut.

Features:
- Displays the source file and line number where the call is coming from.
- Allows to flexibly and easily change the depth of the output trace
- Supports callback functions with a variable number of parameters(up to 8).

## Installation

To install the latest release

```sh
git clone https://gitlab.com/lua_rocks/dprint.git
cd dprint
luarocks make
```
or
```sh
luarocks install https://gitlab.com/lua_rocks/dprint/-/raw/master/dprint-scm-1.rockspec?ref_type=heads
```

```sh
luarocks install dprint
```


## How to use

Simple installation into a project module:
```lua
M = {} -- the current module

local D = require 'dprint'
local dprint = D.dprint

function M.do_stuff(input, data)
  dprint('do_stuff input:', input, 'data:', data)
  -- ... code
end

-- to enable debug mode just call:
-- ... D.enable()
-- to disable debug mode use: D.disable()
```

When you need the ability to turn off the output of debugging information
from one module but not turn it off for all others:

```lua
M = {} -- the current module (with full name 'path/to/my_module')

local D = require 'dprint'
local dprint = D.mk_dprint_for(M) -- create wrapped dprint

function M.do_stuff(input, data)
  dprint('do_stuff input:', input, 'data:', data)
  -- ... code
end

-- D.enable()
-- to disable output from specific module use: D.disable_modules(M) or
-- D.enable_module(M, false)

-- to disable debug output of all modules except the current one(M)
-- D.enable_all_modules(false, { 'path/to/my_module' })
```


#### Another way to turn modules on and off using a mask

to enable|disable modules by module mask:

- to enable all already loaded modules statred with 'stub.os.':
```lua
D.enable_module('stub.os.*', true)
```

- to disable all already loaded modules statred with 'stub.os.':
```lua
 D.enable_module('stub.os.*', false)
-- or
 D.disable_modules('stub.os.*')
```



#### Optional installation.

If the package is installed, it will pick it up and work with it;
if not, it will simply create stubs and continue normal work.

```lua

-- add dprint if found
local ok_dprint, D = pcall(require, 'dprint')
if not ok_dprint then -- make stubs
  local stub = function() end
  D = {
    enable = stub,
    enable_module = stub,
    disable = stub,
    disable_modules  = stub,
    mk_dprint_for = function() return function() end end,
    mk_dvisualize_for = function() return function() end end,
    set_silent_print = function() end,
    get_messages = function() return {} end,
    is_enabled = stub,
    visualize = stub,
    _VERSION = 'stub', -- for using in testing
    _URL = "https://gitlab.com/lua_rocks/dprint",
  }
end
local dprint = D.mk_dprint_for(M)
M.dprint = dprint
M.D = D
```

A more compact version of the minimal stubs:
for src:
```lua
-- luarocks/dprint: pick up if installed or automatically create a stub
local ok_dprint, D = pcall(require, 'dprint') -- debugging print
if not ok_dprint then
  D = {
    _VERSION = 'stub-of-dprint',
    mk_dprint_for = function() return function() end end,
    mk_dvisualize_for = function() return function() end end,
  }
end
local dprint, dvisual = D.mk_dprint_for(M), D.mk_dvisualize_for(M)
--
```

for tests:
```lua
-- luarocks/dprint: pick up if installed or automatically create a stub
local ok_dprint, D = pcall(require, 'dprint')
if not ok_dprint then
  D = { enable = function() end, disable = function() end}
end
--
```

## More examples
See [doc](./doc/examples.md)



## Explanation of how to enable/disable debug output globally and per module

The main idea of `dprint` is to be able to enable debug mode with message output
when needed, while having minimal overhead when using it.

This `dprint` library allows you to enable and disable debug output both
globally for all `dprint` functions and individually for each individual module.

In order to be able to turn off the debugging output for a specific module,
a `dprint`-wrapper should be used during its creation to indicate whether the
debugging output for this module is enabled or disabled.

In order to enable global-scope debug-mode for dprint use:
```lua
  require'dprint'.enable()   --
```

If you need to turn off some modules to reduce the output of debugging
information, you can do it like this:

```lua
  require'dprint'.enable_module(require('my.noisy.module'), false)
  -- noisy module that needs to be turned off
```

But again, this is only possible when the module uses a wrapper over `dprint`:
```lua
-- inside my.noisy.module:
local D = require("dprint")
local dprint = D.mk_dprint_for(M)

function do_stuff(a, b, tbl)
  dprint(1, 'do_stuff a:', a, 'b:', b, ':inspect:', tbl)
  --     ^ trace level up on 1 levels to show who calls this function
  if (a == b) then
    dprint('equal') -- first number of trace level is optional
    -- ...
  end
end
```

When each your module shipped with the `dprint` function wrapper
you can turn the debugging output on and off both globally and locally for this
individual module.

But it is worth considering that in order for debugging information to be output
from an activated module, the global `dprint` output must be enabled.
And modules that are too noisy are turned off separately.

This is how you create a wrapper over the debug output function.
It is a factory to create wrapper over dprint-function.

The main task of which is to provide the ability to turn off the output
for one specific module (just straight at runtime without any code changes)

> part from [source code](./src/dprint.lua)
```lua
local M = {}
-- ..

--
-- turn swith for debug specified module
-- UseCase: be able to turn off the debug output for one specific module
-- for M.mk_dprint_wrapper()
--
---@param module table|string
---@param enable boolean? defualt is true
function M.mk_dprint_for(module, enable)
  assert(type(module) == 'table', 'module must be a table')

  enable = enable == nil and true or enable -- default is on
  enable_module_debug(module, enable)

  local dprint0 = function(...)
    local module0 = module
    -- Condition-Guard to skip output if global dprint is off or if module if off
    -- here M.on - is flag of enabled global scope to output debugging messages
    -- module - ref to the module for which this wrapper was created
    if M.on and (M.modules[module0] or E).debug_enabled then
      local offset, line = M.build_line(...)
      local trace = M.get_trace(offset + 1)
      -- ...
    end
  end
end

return M -- dprint module
```

If the ability to disable a separate module is not needed, you can directly use
the dprint function from the module of the same name without creating
additional wrappers:

```lua
local M = {} -- the current module

local D = require 'dprint'
local dprint = D.dprint

function M.do_stuff(input, data)
  dprint('do_stuff input:', input, 'data:', data)
  -- ... code
end

-- ...

return M

-- to enable debug mode just call: D.enable()
-- to disable debug mode use: D.disable() or D.enable(false)
```

## Example of Passing to dprint a callback call with parameters
## Passing references to functions and parameters for calling them

Here about the feature:
> Supports callback functions with a variable number of parameters(up to 8).

The goal is to make it possible to pass a reference to a callback function and
parameters for it. Instead of calling this function before checking is it needed
to procude debugging output. This is done to avoid doing unnecessary work.
Namely, do not once again produce strings or objects that will
simply be discarded inside the dprint function if debugging is turned off.

The simplest example where something like this might come in handy:

We want to get rid of this kind of cumbersome code:
```lua
function do_stuff(obj, data)
  if D.is_enabled(M) then
    dprint('do_stuff id:', id, Handler._status(obj, true))
  end
  -- ...
end
```

to something like this:
```lua
  dprint('do_stuff id:', id, Handler._status, '(', obj, true, ')')
```


Example:
here callback is a ref to a function
```lua
  dprint('message-1', callback, 'next-key:', val) -- callback without params
  dprint('message-2', callback, '(', arg, ')', 'K:', val) -- one arg for callback

  dprint('message-2', callback, '(', arg1, arg2, argN, ')', 'next-key:', val)
```
Up to 8 parameters are supported for passing to the function.
A sign that after a function there are parameters for calling it is the line `(`

- if there is no closing brace `)`, then all parameters listed after the opening
brace will be passed as a function parameters not as words for build output line.
- if there is no opening bracket at all, the function call will occur without
passing parameters at all. And all values after the reference to this function
will be used as words to build debugging output.

```lua
local M = {} -- module
local dprint = require 'dprint'.mk_dprint_for(M, true)

local function _verbose_output(obj, extra)
  return tostring(obj.field1) .. ' ' .. tostring(obj.field2) .. tostring(extra)
end

function M.do_some_stuff(obj, extra, n)
  dprint('do_some_stuff', _verbose_output, '(', obj, extra, ')', 'n:', n)
  -- insted of:
  -- dprint('do_some_stuff', _verbose_output(obj, extra), 'n:', n)
  -- ..
end
```

See examples in the documentation and tests for more details.
(test: "build_line with functions")


## Why is this?

To save computing resources so that when debugging is disabled,
functions producing debug output are not called at all.

Values are transferred to the function(`dprint`) before it is called, which means
that the `_verbose_output` function will be called first and only then the result
of its work will be transferred inside `dprint`, inside which the debug-mode will
be checked and if debugging is turned off, the values will simply be discarded.

### Alternative way to solve this issue

multiline solution
```lua
if D.is_enabled(M) then
  dprint('c_fprintf fd:', fd, IOHandle._status(h, true))
end
```

An alternative way is to add a check to see if debugging is enabled inside `each`
function that provides detailed debugging information.

```lua
local M = {} -- module
local D = require 'dprint'
local dprint = D.mk_dprint_for(M, true)

-- debug output producer
local function _verbose_output(obj, extra)
  if not  D.is_enabled() then return end -- skip produce debug output

  -- produce debug output
  return tostring(obj.field1) .. ' ' .. tostring(obj.field2) .. tostring(extra)
end


function M.do_some_stuff(obj, extra, n)
  dprint('do_some_stuff', _verbose_output(obj, extra), 'n:', n)
  -- ...
end
```


