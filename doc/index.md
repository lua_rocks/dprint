## Overview

The simplest debugging information logger.
Disabled by default, with ability to enable debug mode at any time.
When debug mode is enabled, data is simply output to StdOut.


## How to use

Simple installation into a project module:
```lua
M = {} -- the current module

local D = require 'dprint'
local dprint = D.dprint

function M.do_stuff(input, data)
  dprint('do_stuff input:', input, 'data:', data)
  -- ... code
end

-- to enable debug mode just call:
-- ... D.enable()
-- to disable debug mode use: D.disable()
```

When you need the ability to turn off the output of debugging information
from one module but not turn it off for all others:

```lua
M = {} -- the current module (with full name 'path/to/my_module')

local D = require 'dprint'
local dprint = D.mk_dprint_for(M) -- create wrapped dprint

function M.do_stuff(input, data)
  dprint('do_stuff input:', input, 'data:', data)
  -- ... code
end

-- D.enable()
-- to disable output from specific module use: D.disable_modules(M) or
-- D.enable_module(M, false)

-- to disable debug output of all modules except the current one(M)
-- D.enable_all_modules(false, { 'path/to/my_module' })
```

.. See README.md
