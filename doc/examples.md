## Usage Example: Quick activation in tests + wrapped dprint for module

An example of how you can quickly catch your own mistake in tests using a dprint
usefull when active development is underway and sometimes you need to quickly
look at the progress of code execution without turning to the debugger.

- create wrapped dprint for specific module (1) or use global dprint
- use `:inspect:` before variable to see values of table (2)
- turn on the output for all modules (3)

spec-file:
```lua
require("busted.runner")()
local assert = require("luassert")

local M = require("util.docker");

describe("util.docker", function()
  it("fmt_images", function()
    local images = { { 'alpine', '3.19', 'f8c20f8bbcb6', '7.38MB' } }
    local max = { repo = 8, tag = 12, size = 6 }
    local exp = '     2    alpine 3.19          f8c20f8bbcb6  7.38MB'

    require 'dprint'.enable() -- <<<(3) add this line to activate dprint

    assert.same(exp, M.fmt_images(images, max))
  end)
end)
```


util.docker file:
```lua
local M = {} -- module
-- wrapper for this module
local dprint = require 'dprint'.mk_dprint_for(M, true) --  <<<(1) create wrapper
-- or local dprint = require 'dprint'.dprint

-- ...

---@param images table
---@param max table
function M.fmt_images(images, max)
  dprint(':inspect:', max) --                 <<<(2)  added to see own mistakes
  local res = ''

  for i, t in ipairs(images) do
    dprint(':inspect:', t) --                    <<<  added to see own mistakes
    res = res .. string.format("%s...", i, t.repo, ...) -- fails
  end

  return res
end

return M
```

Output:
```
spec/util/docker_spec.lua:89: # Debug Print Enabled print:  |_G.print|
src/util/docker.lua:41: :inspect: <1>{repo = 8, size = 6, tag = 12}
src/util/docker.lua:48: :inspect: <1>{[1] = "alpine", [2] = "3.19", [3] = "f8c20f8bbcb6", [4] = "7.38MB"}
```


## Function
## Passing references to functions and parameters for calling them

Here about the feature:
> Supports callback functions with a variable number of parameters

The goal is to make it possible to pass a reference to a callback function and
parameters for it. Instead of calling this function before checking is it needed
to procude debugging output. This is done to avoid doing unnecessary work.
Namely, do not once again produce strings or objects that will
simply be discarded inside the dprint function if debugging is turned off.


```lua
local M = {} -- module
local dprint = require 'dprint'.mk_dprint_for(M, true)

-- ...

---@param obj table
local function _verbose_output(obj, extra)
  return tostring(obj.field1) .. ' ' .. tostring(obj.field2) .. tostring(extra)
end

local state = {}
function M._status()
  return tostring(state)
end

function M.do_some_stuff(obj, extra, n)
  -- dprint('do_some_stuff', _verbose_output(obj, extra), 'n:', n)
  dprint('do_some_stuff', _verbose_output, '(', obj, extra, ')', 'n:', n)
  --                      ^^ callback  ^^       ^        ^        ^
  --                         function      params for function    |
  local key = 8 -- ..                                            /
  dprint('status:', _status, 'key:', key) --                   /
  --                           ^ next-word to build the output line
  --                ^ call without params
  -- ...

  return res
end

return M -- module
```
